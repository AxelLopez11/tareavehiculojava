/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Jarvin
 */
public class DatosAutomovil implements Serializable{
    private String Marca;
    private String Modelo;
    private String Año;
    private String Color;
    private String Chasis;
    private String Placa;
    private String VIN;
    private String FechaEmisionCirculacion;
    private String Gravamen;
    private String NumPasajeros;
    private String Tonelaje;
    private String NumCilindros;
    private String Combustible;
    private String TipoUso;
    private String Servicio;

    public DatosAutomovil() {
    }

    public DatosAutomovil(String Marca, String Modelo, String Año, String Color, String Chasis, String Placa, String VIN, String FechaEmisionCirculacion, String Gravamen, String NumPasajeros, String Tonelaje, String NumCilindros, String Combustible, String TipoUso, String Servicio) {
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.Año = Año;
        this.Color = Color;
        this.Chasis = Chasis;
        this.Placa = Placa;
        this.VIN = VIN;
        this.FechaEmisionCirculacion = FechaEmisionCirculacion;
        this.Gravamen = Gravamen;
        this.NumPasajeros = NumPasajeros;
        this.Tonelaje = Tonelaje;
        this.NumCilindros = NumCilindros;
        this.Combustible = Combustible;
        this.TipoUso = TipoUso;
        this.Servicio = Servicio;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getAño() {
        return Año;
    }

    public void setAño(String Año) {
        this.Año = Año;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getChasis() {
        return Chasis;
    }

    public void setChasis(String Chasis) {
        this.Chasis = Chasis;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getFechaEmisionCirculacion() {
        return FechaEmisionCirculacion;
    }

    public void setFechaEmisionCirculacion(String FechaEmisionCirculacion) {
        this.FechaEmisionCirculacion = FechaEmisionCirculacion;
    }

    public String getGravamen() {
        return Gravamen;
    }

    public void setGravamen(String Gravamen) {
        this.Gravamen = Gravamen;
    }

    public String getNumPasajeros() {
        return NumPasajeros;
    }

    public void setNumPasajeros(String NumPasajeros) {
        this.NumPasajeros = NumPasajeros;
    }

    public String getTonelaje() {
        return Tonelaje;
    }

    public void setTonelaje(String Tonelaje) {
        this.Tonelaje = Tonelaje;
    }

    public String getNumCilindros() {
        return NumCilindros;
    }

    public void setNumCilindros(String NumCilindros) {
        this.NumCilindros = NumCilindros;
    }

    public String getCombustible() {
        return Combustible;
    }

    public void setCombustible(String Combustible) {
        this.Combustible = Combustible;
    }

    public String getTipoUso() {
        return TipoUso;
    }

    public void setTipoUso(String TipoUso) {
        this.TipoUso = TipoUso;
    }

    public String getServicio() {
        return Servicio;
    }

    public void setServicio(String Servicio) {
        this.Servicio = Servicio;
    }
    
    
}
